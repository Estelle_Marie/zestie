<?php

namespace App;
use App\Entity\Candidate;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Security\Core\Authentication\Provider\UserAuthenticationProvider;



class CompletProfil
{
    
    public function completedProfil()
    {
        //on récupère l'utilisateur connecté
        //global $Kernel;
        //$securityContext = $Kernel->getContainer()->get('security.context')->getToken()->getUser();

        // $candiate = $securityContext->getToken()->getUser();
        // $container = $this->configurationPool->getContainer();
        
        // $candidate = $securityContext->getToken()->getUser();
        //dd($securityContext);
        dd($userProvider->getUser());

        // tout les champs que nous devons complèter
        $shouldBeCompletedFields = [ 'gender', 'firstName', 'lastName', 'adress', 'country',  'nationality', 'email', 'cv', 'passport', 'profilPicture', 'currentLocation', 'dateOfBirth', 'placeOfBirth', 'jobSelector', 'experience', 'shortDescription', 'availability'];

        //le compteur de champs remplie
        $completedFields = 0;
        //par défaut tout les champs sont remplis
        $ifNotComplete = false;

        //on parcours le tableau de tout les champs qui doivent être remplies
        foreach ($shouldBeCompletedFields as $filed) {
            //les méthode get doivent être en maj getFirstName
            $property = "get".ucfirst($filed);
            //si les méthodes get de candidate ne sont pas complété
            if(!$candidate->$property()) {
                // alors isNotComplete devient true
                $ifNotComplete = true;
            } else {
                //sinon on fait +1 à notre compteur
                $completedFields ++;
            }
        }

        // l'arrondi de : completedFields(notre compteur) div par shouldBeCompletedFields(tout les champs du tab) * 100 (le pourcentage)
        $pourcentageProfil = round(($completedFields / count($shouldBeCompletedFields)) *100);

        
        return $pourcentageProfil;
    }
}
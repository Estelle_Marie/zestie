<?php

namespace App\Controller;

use App\Entity\Candidate;
use App\Form\CandidateType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\Serializer\SerializerInterface ;
use App\Entity\JobOffer;
use App\CompletProfil;

/**
 * @Route("/candidate")
 */
class CandidateController extends AbstractController
{
    /**
     * @Route("/", name="candidate_index", methods="GET")
     */
    public function index(): Response
    {
        $candidates = $this->getDoctrine()
            ->getRepository(Candidate::class)
            ->findAll();

        return $this->render('candidate/index.html.twig', ['candidates' => $candidates]);
    }

    /**
     * @Route("/new", name="candidate_new", methods="GET|POST")
     */
    public function new(Request $request): Response
    {
        $candidate = new Candidate();
        $form = $this->createForm(CandidateType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($candidate);
            $em->flush();

            return $this->redirectToRoute('candidate_index');
        }

        return $this->render('candidate/new.html.twig', [
            'candidate' => $candidate,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/profil", name="profil", methods="GET")
     */
    public function profil(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $candidate = $this->getUser();
        
        //$candidate = new Candidate;
        return $this->render('candidate/edit.html.twig', ['candidate' => $candidate]);
    }

    /**
     * @Route("/edit", name="edit", methods="GET|POST")
     */
    public function edit(Request $request, CompletProfil $completProfil): Response
    {
        // on récupère l'utilisateur connecté
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $candidate = $this->getUser();
        //$candidate = $completProfil->completedProfil();

        //méthode champs complèter à 100%
        $completedFields = $this->completedProfil();
        
        //on récupère les anciennes données profil, cv et passport de notre bdd
        $getCv = $candidate->getCv();
        $getPassport = $candidate->getPassport();
        $getPicture = $candidate->getProfilPicture();

        // on crée le formulaire  et vérifie les champs
        $form = $this->createForm(CandidateType::class, $candidate);
        $form->handleRequest($request);

        // si on submit et on valide le formulaire
        if ($form->isSubmitted() && $form->isValid()) {
            
            // Upload un fichier
            $fileCv = $candidate->getCv();
            $filePassport = $candidate->getPassport();
            $filePicture = $candidate->getProfilPicture();
            
            // si on ne met un fichier dans les champs ... 
            if($fileCv != NULL)
            {
                // On utilise la méthode correspondante pour Upload
                $this->uploadCv();
            } else {
                //sinon on récupère l'ancienne valeur 
                $candidate->setCv($getCv);
            }

            if($filePassport != NULL)
            {
                $this->uploadPassport();
            } else {
                $candidate->setPassport($getPassport);
            }

            if($filePicture != NULL)
            {
                $this->uploadPicture();
            } else {
                $candidate->setProfilPicture($getPicture);
            }

            //enregistrement des champs
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            $this->addFlash('message', 'Your changes were saved!');
            //redirection
            return $this->redirectToRoute('edit');
            //return $this->redirect($this->generateUrl('app_product_list'));
            
            
        }

        return $this->render('candidate/edit.html.twig', [
            'candidate' => $candidate,
            'formProfil' => $form->createView(),
            'completedFields' => $completedFields            
        ]);

    }

    public function uploadCv (){
        // on instancie l'objet
        $candidateCv = $this->getUser();

        // Upload un fichier
        $fileCv =$candidateCv->getCv();
        
        // Récupéré le nom des fichiers
        $fileNameCv = $this->generateUniqueFileName().'.'.$fileCv->guessExtension();
        
        //déplace le fichier dans le répertoire où sont stockées les brochures
        $fileCv->move(
        $this->getParameter('cv_directory'),
        $fileNameCv);

        // On Updates le new fichier 
        $candidateCv->setCv($fileNameCv);
    }

    public function uploadPassport (){
        
        // on instancie l'objet
        $candidatePassport= $this->getUser();

        // Upload un fichier    
        $filePassport =$candidatePassport->getPassport();
        // Récupéré le nom des fichiers
        $fileNamePassport = $this->generateUniqueFileName().'.'.$filePassport->guessExtension();        
        //déplace le fichier dans le répertoire où sont stockées les brochures
        $filePassport->move(
        $this->getParameter('passport_directory'),
        $fileNamePassport);

        // On Updates le new fichier 
        $candidatePassport->setPassport($fileNamePassport);

    }

    public function uploadPicture (){

        // on instancie l'objet
        $candidatePicture = $this->getUser();

        // Upload un fichier
        $filePicture = $candidatePicture->getProfilPicture();
        // Récupéré le nom des fichiers
        $fileNamePicture = $this->generateUniqueFileName().'.'.$filePicture->guessExtension();
        
        //déplace le fichier dans le répertoire où sont stockées les brochures
        $filePicture->move(
            $this->getParameter('picture_directory'),
            $fileNamePicture);

        // On Updates le new fichier 
        $candidatePicture->setProfilPicture($fileNamePicture);
    
    }

    // la méthode pour encoder le fichier et générer qu'un seul nom 
    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }


    public function completedProfil()
    {
        //on récupère l'utilisateur connecté
        $candidate = $this->getUser();

        // tout les champs que nous devons complèter
        $shouldBeCompletedFields = [ 'gender', 'firstName', 'lastName', 'adress', 'country',  'nationality', 'email', 'cv', 'passport', 'profilPicture', 'currentLocation', 'dateOfBirth', 'placeOfBirth', 'jobSelector', 'experience', 'shortDescription', 'availability'];

        //le compteur de champs remplie
        $completedFields = 0;
        //par défaut tout les champs sont remplis
        $ifNotComplete = false;

        //on parcours le tableau de tout les champs qui doivent être remplies
        foreach ($shouldBeCompletedFields as $filed) {
            //les méthode get doivent être en maj getFirstName
            $property = "get".ucfirst($filed);
            //si les méthodes get de candidate ne sont pas complété
            if(!$candidate->$property()) {
                // alors isNotComplete devient true
                $ifNotComplete = true;
            } else {
                //sinon on fait +1 à notre compteur
                $completedFields ++;
            }
        }

        // l'arrondi de : completedFields(notre compteur) div par shouldBeCompletedFields(tout les champs du tab) * 100 (le pourcentage)
        $pourcentageProfil = round(($completedFields / count($shouldBeCompletedFields)) *100);

        
        return $pourcentageProfil;
    }

    /**
     * @Route("/{id}", name="candidate_delete", methods="DELETE")
     */
    public function delete(Request $request, Candidate $candidate): Response
    {
        if ($this->isCsrfTokenValid('delete'.$candidate->getId(), $request->request->get('_token'))) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($candidate);
            $em->flush();
        }

        return $this->redirectToRoute('candidate_index');
    }

    /**
     * @Route("/{title}/{id}/apply", name="apply", methods="GET")
     */
    public function apply(JobOffer $jobOffer) : Response
    {
        $candidate = $this->getUser();
        $completedFields = $this->completedProfil();

        
        

        return $this->redirectToRoute('show', compact('completedFields'));
    }
}